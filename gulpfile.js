const gulp = require('gulp');
const log = require('fancy-log');
const critical = require('critical').stream;
const purgecss = require('gulp-purgecss')
const imagemin = require('gulp-imagemin');
const del = require('del');
const BUILD_TO = './dist/';
const CSS_TO_PURGE = ['css/bootstrap.min.css', 'css/material.min.css'].map(link => BUILD_TO + link);
const HTML_TO_INLINE = ['*.html'].map(link => BUILD_TO + link);

// Generate & Inline Critical-path CSS
const gulp_critical = function() {
    return gulp
        .src(HTML_TO_INLINE, {base: BUILD_TO})
        .pipe(critical({inline: true}))
        .on('error', function(err) {
            log.error(err.message);
        })
        .pipe(gulp.dest(BUILD_TO));
}

// Shrinks css by removing unused lines
const gulp_purgecss = function() {
    return gulp
        .src(CSS_TO_PURGE, {base: BUILD_TO})
        .pipe(purgecss({content: ['*.html', '**/*.html', 'js/*.js'].map(link => BUILD_TO + link)}))
        .on('error', function(err) {
            log.error(err.message);
        })
        .pipe(gulp.dest(BUILD_TO));
};

const gulp_clone = function() {
    return gulp
        .src('./src/' + '**/*')
        .pipe(gulp.dest(BUILD_TO));
};

// Minifies images.
const gulp_imagemin = function() {
    return gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
}

const gulp_delete = function () {
    return del([
        'dist/**/*',
    ]);
}

gulp.task('build',
    gulp.series(gulp_delete, gulp_clone, gulp_purgecss, gulp_critical, gulp_imagemin)
);

gulp.task('critical',
    gulp.series(gulp_delete, gulp_clone, gulp_critical)
);